#include <iostream>
#include <fstream>

using namespace std;

int insertToLog(int x){
    fstream myfile;
    myfile.open ("transaction_log.txt", std::ios_base::app);
    if(myfile.tellp() == 0)
        myfile << x;
    else
        myfile << "\n" << x;
    myfile.close();
    return 2;
}

int insertToMain(int x){
    ifstream f("main.txt");
    char s[10];
    int d;
    int i = 0;
    while(f >> d){
        i++;
    }
    if(i > 0 && x < d)
        return insertToLog(x);

    ofstream myfile;
    myfile.open ("main.txt", std::ios_base::app);
    if(i == 0)
        myfile << x;
    else
        myfile << "\n" << x;
    myfile.close();
    return 1;
}

int insert(int x){
    return insertToMain(x);
}

int search(int x){
    ifstream f("main.txt");
    string d;
    while(f >> d){
        if(d == to_string(x))
            return 1;
    }

    ifstream f2("transaction_log.txt");
    while(f2 >> d){
        if(d == to_string(x))
            return 2;
    }
    return 0;
}

int deleteData(int x){
    ifstream f("main.txt");
    string d;
    string temp = "";
    int file = 0;
    while(f >> d){
        if(d == to_string(x)){
            file = 1;
            temp.append("*"+d+"\n");
        }else{
            temp.append(d+"\n");
        }
    }
    f.close();

    if(file == 0){
        temp = "";
        f.open("transaction_log.txt");
        while(f >> d){
            if(d == to_string(x)){
                file = 2;
                temp.append("*"+d+"\n");
            }else{
                temp.append(d+"\n");
            }
        }
        f.close();
    }

    ofstream mFile;
    if(file == 1)
       mFile.open("main.txt");
    else if(file == 2)
        mFile.open("transaction_log.txt");
    
    if(file == 1 || file == 2){
        mFile << temp;
        mFile.close();
    }
    return file;
}

void update(int old, int newData){
    int del = deleteData(old);
    if(del == 1 || del == 2)
        insert(newData);
}

int main(){
    int x, menu = 0, s, oldData;
    while(menu != -1){
        cout << "1. Insert\n"
            << "2. Search\n"
            << "3. Update\n"
            << "4. Delete\n"
            << "5. Exit\n"
            << "Pilih menu : ";
        cin >> menu;

        switch(menu){
            case 1:
                cout << "Enter a number : "; 
                cin >> x;
                if(insert(x) == 1)
                    cout << x << " inserted to main.txt";
                else
                    cout << x << " inserted to transaction_log.txt";
            break;
            case 2:
                cout << "Enter a number : "; 
                cin >> x;
                s = search(x);
                if(s == 1)
                    cout << "data ditemukan di main.txt" << endl;
                else if(s == 2)
                    cout << "data ditemukan di transaction_log.txt" << endl;
                else
                    cout << "data tidak ditemukan" << endl;
            break;
            case 3:
                cout << "Enter old data : "; 
                cin >> oldData;
                cout << "Enter new data : "; 
                cin >> x;
                update(oldData, x);
            break;
            case 4:
                cout << "Enter a number : "; 
                cin >> x;
                deleteData(x);
            break;
            default:
                menu = -1;
                break;
        }
        cout << endl << endl;
    }
    return 0;
}